[TDD Part 1](https://learning-oreilly-com.eznvcc.vccs.edu:2443/videos/clean-code/9780134661742/9780134661742-CODE_01_05_01)

## What are the Three Laws of Test-driven Development?
1. Write **NO** production code except to pass a failing test
2. Write only **enough** of a test to demonstrate a failure
3. Write only **enough** production code to pass the currently failing test
## Explain the Red -> Green -> Refactor -> Red process.
step by step approach to writing tests and keeping code clean
## What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).
Rotting code is rigid, fragile, and immobile.
## Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?
If you try to clean it, you might break something. Tests let you clean your code without fear. You make a change, run the tests, and continue. If something breaks you know where to look. 
## Uncle Bob mentions [FitNesse](http://fitnesse.org/) as an example of a project that uses TDD effectively. What is FitNesse? What does it do?
FitNesse automated acceptance tests help you build code. 
## What does Uncle Bob say about a program with a long bug list? What does he say this comes from?
He says a program with a long bug list comes from irresponsibility and carelessness. 
## What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?
complete and reliable low level documentation
improved design
