Notes:
ep 9 single responsbility principle
responisibility family of fuctions
module with more than one responsibility fragile due to interactions

Open closed principle
in the book
open for extension but closed for modification
very simple to change the behavior
source code should not change
Abstraction and inversion
abstract interface, invert the dependencies
add new code, not change old code
if it doesn't change it can't rot
Often not possible
main should be separate
main can't be with open closed principle
crystal ball problem
Maybe not entire systems, but functions, classes and small components
small things can use open closed principle

extract methods to make code better
dont want closed for extension and open for modification
when you change a thing anything that depends on it also affected
rigidity, small design change huge impact, system resist change
fragility, by adding it breaks something
so many plugins to sell, no impossible because they are connected it can't be seperate and cut up
immobility, we can't do things because of how the system is 

customers do the unexpected
we don't predict the future
open closed principle only protects you from change if you can predict the future
1. think of everything Big Design Up Front BDUF, creates large complex designs that are unnecessary
changing an overengineered design harder than a simple one
2. agile design
pragmatic- realistic
reactive- showing response to
look at past change for future change
make abstractions for protection in future that type of change
no design upfront, no
we want basic outline
abstractions hard to maintain
user story in iteration zero don't need to be correct they are just a start and then you can get changes after
minimize change, not get rid
well enough, not perfect

Questions:
1. Uncle Bob begins with a short summary of the principle discussed in Episode 9. What is that principle?
single responsibility principle to make it not fragile.

2. In the fascinating segments Uncle Bob always starts his videos with, we learn about the origin of Planck's constant. How did Planck stumble upon this constant and what lesson of discovery does it reveal?
1990 tried a trick he read about, assumed energy of gas molecules was quantized for approximation. Planck assumed there was a constant of proportionality, it worked very well. It is Planck's law.
if you are curious, try to see if something works, worst senario it doesn't

3. Describe the two goals of open-closed principle introduced by Bertrand Meyer in his classic 1988 book.
open for extension but closed for modification
change things by adding but leave the source code alone.

4. This episode is filled with "keepers", but I found this gem particularly worth noting: "The very things that make abstractions useful to us also make them costly". Explain what Uncle Bob means by this statement.
abstractions are complicated, having too many makes things stop making sense, 
they are useful to make complicated and powerful things, but they are hard to maintain because they are complicated.
