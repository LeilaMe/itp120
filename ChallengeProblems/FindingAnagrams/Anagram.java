import java.util.Scanner;

class Anagram {
    public static void main(String[] args) {
        String letters = "abcdefghijklmnopqrstuvwxyz";
        Scanner scanner = new Scanner(System.in);
        int counter = 0;
        int sentence[] = new int[26];

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            int countLetters[] = new int[26];

            for(int i = 0;i < line.length(); i++){
                char letter = line.charAt(i);
                int index = letters.indexOf(letter);
                if(index >= 0){
                    if(counter == 0){
                        sentence[index]++;
                    }
                    countLetters[index]++;
                }
            }
            for(int i = 0; i< 26; i++){ 
                if(countLetters[i] > sentence[i]){
                    System.out.println(i+" "+countLetters[i]+" "+sentence[i]);
                    System.out.println("No anagrams for this sentence");
                    break;
                    }
                else if(i == 25){
                    System.out.println(line);
                }
            }
            counter++;
        }
    }
} 
