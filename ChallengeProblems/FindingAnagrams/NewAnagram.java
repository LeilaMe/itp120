import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

class NewAnagram 
{   
    public static int[] addword(int[] one, int[] two) 
    {
        int[] three = new int[26];
        for(int i = 0; i < 26 ; i++)
        {
            three[i] = one[i] + two[i];
        }
        return three;
    }
    
    public static int[] subtractword(int[] one, int[] two)
    {
        int[] three = new int[26];
        for(int i = 0; i < 26; i++)
        {
            three[i] = one[i] - two[i];
        }
        return three;
    }

    public static void main(String[] args) 
    {
        String letters = "abcdefghijklmnopqrstuvwxyz";
        Scanner scanner = new Scanner(System.in);
        int counter = 0;
        int sentence[] = new int[26];
        int totalCount[] = new int[26];
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<int[]> amounts = new ArrayList<int[]>();
        
        while (scanner.hasNextLine()) 
        {
            String line = scanner.nextLine();
            words.add(line);
            int[] countLetters = new int[26];
            
            for(int i = 0;i < line.length(); i++)
            {
                int index = letters.indexOf(line.charAt(i));
                if((index >= 0) && (counter != 0))
                    {
                        countLetters[index]++;
                    }
            }
            amounts.add(countLetters);
            counter++;
        } 
            /*System.out.println(Arrays.toString(sentence));
            System.out.println(Arrays.toString(totalCount));
            System.out.println(Arrays.toString(countLetters));*/
        
        //System.out.println(Arrays.toString(words.toArray()));
        for(String line: words)
        {   
            if(words.get(0) == line)
            {
                for(int i = 0; i<line.length(); i++)
                {
                    int index = letters.indexOf(line.charAt(i));
                    if(index >= 0)
                    {
                    sentence[index]++;
                    }
                }
            }
            else
            {
                for(int i = 0;i< line.length(); i++)
                {
                    int index = letters.indexOf(line.charAt(i));
                    totalCount[index]++;
                }
            }
            
        }
    
        //System.out.println(Arrays.toString(amounts.toArray()));
        //System.out.println(Arrays.toString(sentence));
        //System.out.println(Arrays.toString(totalCount));

        //String attempt;
        int[] attempt = new int[26];
        //for(int a = 0; a < words.size(); a++)
            //{
        String message = "";
        int a = 1;
        while((attempt != sentence) && (a< words.size()))
        {
            //attempt = attempt + " " + words[a];
            attempt = addword(attempt, amounts.get(a));
            message = message + " " + words.get(a);
            for(int i = 0; i< attempt.length; i++)
            {
                if(attempt[i] > sentence[i])
                {
                    attempt = subtractword(attempt, amounts.get(a));
                    message = message.replaceFirst((" " + words.get(a)), "");
                }
            }
            a++;
            //System.out.println(Arrays.toString(attempt));
        }
        System.out.println(message);
    }
} 
