import java.util.Scanner;
import java.lang.*;

class CaesarCiper{
    public static boolean isOver(String line){
        boolean done = line.equals("STOP");
        System.out.println("");
        return done;
    }
    
    public static void cipher() { 
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int counter = 0;
        int key = 0;

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(isOver(line) == true){
                break;
            }
            if(counter == 0){
                key = Integer.parseInt(line);
            }
            else{
                for(int i = 0; i< line.length(); i++) {
                    char character = line.charAt(i);
                    int newindex;
                    if (alphabet.contains(Character.toString(character))){
                        int index = alphabet.indexOf(character);
                        newindex = (index + key) % 26;
                        System.out.print(alphabet.charAt(newindex));
                    }
                    else if(ALPHABET.contains(Character.toString(character))){
                        int index = ALPHABET.indexOf(character);
                        newindex = (index + key) % 26;
                        System.out.print(ALPHABET.charAt(newindex));
                    }
                    else{
                        System.out.print(character);
                    }
                }
            }
            counter++;
        }
    }
    public static void main(String[] args) { 
        cipher();
    }
}
