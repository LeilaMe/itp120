import java.util.Scanner;
import java.util.ArrayList;

class Quidditch {
    public static void main(String[] args) {
        String[] housesArray = new String[4];
        housesArray[0] = "Gryffindor";
        housesArray[1] = "Ravenclaw";
        housesArray[2] = "Slytherin";
        housesArray[3] = "Hufflepuff";
        
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> regArray = new ArrayList<String>();
        ArrayList<Integer> count = new ArrayList<Integer>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String a = new String(line);
            regArray.add(a);
        }
        
        for(String house : housesArray) {
            int ct = 0;
            for(String line : regArray){
                if(line.contains(house)){
                    ct++;
                }
            }
            count.add(ct);
        }
        
        ArrayList<Integer> checker = new ArrayList<Integer>();
        for(int e = 0; e < 4; e++){
            checker.add(7);
        }
        if(count.equals(checker)){
            System.out.println("List complete, let's play quidditch!");
            return;
        }

        for(int i = 0 ; i < 4; i++) {
            if(count.get(i) < 7){
                System.out.println(housesArray[i] + " does not have enough players.");
            }
            else if(count.get(i) > 7){
                System.out.println(housesArray[i] + " has too many players.");
            }
            else{
                System.out.println(housesArray[i] + " has 7.");
            }
        }
    }
}
