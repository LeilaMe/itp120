import java.util.ArrayList;

public class SimpleDotCom {
  private int hits = 0;
  private int guesses = 0;

  private ArrayList<String> locations;
/*
  public SimpleDotCom() {
      int start = (int)(Math.random() * 5);
      locations = new ArrayList<String>(start, start + 1, start + 2);
  }
*/
  public void setLocationCells(ArrayList<String> loc) {
      locations = loc;
  }

  public boolean over()
  {
      return hits > 2;
  }

  public String makeMove(String userInput) {
      int guess = Integer.parseInt(userInput);

      guesses++;
      System.out.println(this.locations);
/*
      for (int location : locations) {
          if (location == guess) {
              this.hits++;
              if (this.hits > 2)
                  return "kill";
              return "hit";
          }
      }
      return "miss";*/
      String result = "miss";
      int index = locations.indexOf(guess);
      if (index >= 0) {
          locations.remove(index);
          hits++;
          if (locations.isEmpty()){
              result = "kill";
          }
          else{
              result = "hit";
          }
      }
      return result;
  }
  public String displayResults() {
      return "You took " + guesses + " guesses.";
  }
}
