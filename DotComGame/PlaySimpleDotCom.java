public class PlaySimpleDotCom {
  public static void main(String[] args) {
      SimpleDotCom game = new SimpleDotCom();
      CliReader reader = new CliReader();

      while (!game.over()) 
      {
          System.out.println(
                  game.makeMove(reader.input("Enter a number: "))
                  );
      }
      System.out.println(game.displayResults());
  }
}
