
/**
 * Write a description of class Cracker here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Cracker implements Edible
{
    // instance variables - replace the example below with your own
    private int weight;
    public int getweight()
    {
        return weight;
    }
    
    /**
     * Constructor for objects of class Cracker
     */
    public Cracker()
    {
        // initialise instance variables
        weight = 2;
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public int bite()
    {
        weight = weight - 1;
        return weight;
    }
}
