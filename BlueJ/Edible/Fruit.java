
/**
 * Abstract class Fruit - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Fruit implements Edible
{
    public int weight;
    public int getweight()
    {
        return weight;
    }

    public int bite()
    {
        weight = weight - 3;
        return weight;
    }
}
