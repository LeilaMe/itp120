
/**
 * Write a description of class FruitTest here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class EatTest extends Fruit
{

    public static void main(String [] args)
    {
        // initialise instance variables
        Edible[] i = new Edible[3];
        i[0] = new Apple();
        i[1] = new Egg();
        i[2] = new Cracker();
        for(int x = 0; x < 3; x++)
        {
            System.out.println("weight before: "+ i[x].getweight());
            System.out.println("after bite: " + i[x].bite());
        }
    }
}
