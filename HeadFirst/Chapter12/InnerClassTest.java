class InnerClassTest{
    
    private int x = 19;
    InnerClass inner = new InnerClass();
    public void testOne() {
        System.out.println("testOne");
        inner.go();
    }

    class InnerClass{
        void go() {
            x = 21;
            System.out.println(x);
        }
    }
}
