import javax.swing.*;
import java.awt.event.*;

public class SimpleGui1B implements ActionListener {
    JButton button;

    public static void main (String[] args) {
        SimpleGui1B gui = new SimpleGui1B();
        gui.go();
    }

    public void go() {
        JFrame frame = new JFrame();
        button = new JButton("click me");

        button.addActionListener(this);
        //add me to your list of listeners

        frame.getContentPane().add(button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //makes program quit when you close window
        frame.setSize(300, 300);
        //size in pixels
        frame.setVisible(true);
    }
//this is the event-handling method
    public void actionPerformed(ActionEvent event) {
        button.setText("I've been clicked hahaha");
    }
}
