public class MonsterTestDrive {
    public static void main(String [] args) {
        Monster [] ma = new Monster[3];
        ma[0] = new Vampire();
        ma[1] = new Dragon();
        ma[2] = new Monster();
        for(int x = 0; x < 3; x++) {//cycles through each index
            ma[x].frighten(x);
            //does frighten method of the monster at the index
        }
    }
}

class Monster{
    boolean frighten(int d) {
        System.out.println("arrrgh");
        return true;
    }
}

class Vampire extends Monster {
    boolean frighten(int x) {
        System.out.println("a bite?");
        return false;
    }
}
//Answer 4: all booleans, all frighten method, all return true
//Answer 1: all booleans, all returns are boolean
//i get it now. a bite, false. never mind I don't get it

class Dragon extends Monster {
    boolean frighten(int degree) {
        System.out.println("breath fire");
        return true;
    }
}
