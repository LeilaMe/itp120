class Socks{
    public Socks(boolean smells) {
        System.out.println("It is " + smells + " that the socks smell");
    }
    public Socks(){
        System.out.println("SOCKS!");
    }
    public Socks(String name) {
        System.out.println("Socks name is " + name);
    }
}

public class TestSocks{
    public static void main(String[] args){
        Socks a = new Socks();
        Socks b = new Socks("Soup");
        Socks c = new Socks(true);
    }
}
