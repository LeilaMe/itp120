class Beanie{
    public Beanie(){
        this(4);
    }
    public Beanie(int size){
        System.out.println("This beanie is size " + size);
    }
}

public class Hat{
    public static void main(String[] args){
        Beanie bean = new Beanie();
    }
}
