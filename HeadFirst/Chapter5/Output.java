class Output {
    public static void main(String [] args) {
        Output o = new Output();
        o.go();
    }
    void go() {
        int y=7;
        for(int x = 1; x < 8; x++) {
            y++;
            if (x>4){
                System.out.print(++y + " ");
            }
            if (y > 14) {
                System.out.println(" x = " + x);
                break;
            }
        }
    }
}
/* i think it will output 12 14 
y=7
x=1  y=8
x=2  y=9
x=3  y=10
x=4  y=11
x=5  y=12
prints out 13 
x=6  y=13
prints out 14?
x=7  y=14 
prints out 15?
*/
/* 
   it will be the last option
   y = 7
   x = 1 y = 8
   x = 2 y = 9
   x = 3 y = 10
   x = 4 y = 11
   x = 5 y = 12 y = 13 prints out 13
   x = 6 y = 14 y = 15 prints out 15 prints out x = 6 break
*/
