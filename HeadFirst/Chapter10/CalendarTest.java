import java.util.Calendar;

class CalendarTest{
    public static void main(String[] args){
        Calendar myCal = Calendar.getInstance();
        
        myCal.set(2004, 6, 1, 12, 34);
        System.out.println(myCal.getTime());

        long day1 = myCal.getTimeInMillis();
        day1 += 1000 * 60 * 60;
        myCal.setTimeInMillis(day1);
        System.out.println("new hour " + myCal.get(myCal.HOUR_OF_DAY));
        
        myCal.add(myCal.DATE, 35);
        System.out.println("add 35 days " + myCal.getTime());

        myCal.roll(myCal.DATE, 35);
        System.out.println("roll 35 days " + myCal.getTime());

        myCal.set(myCal.DATE, 1);
        System.out.println("set to 1 " + myCal.getTime());
    }
}
