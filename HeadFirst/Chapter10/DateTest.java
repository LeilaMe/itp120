import java.util.Date;

class DateTest{
    public static void main(String[] args){
        Date today = new Date();

        System.out.println(String.format("%tc", new Date()));
        System.out.println(String.format("%tr", new Date()));
        System.out.println(String.format("%tA, %<tB %<td", today));
    }
}
