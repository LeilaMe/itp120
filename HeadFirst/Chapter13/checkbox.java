import javax.swing.*;
import java.awt.event.*;

public class checkbox implements ItemListener{
    JFrame frame;
    JCheckBox check;

    public static void main (String[] arg) {

        checkbox gui = new checkbox();
        gui.go();
    }

    public void go() {
        frame = new JFrame();

        check = new JCheckBox("<-- my check box");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(check);
        frame.setSize(300,300);
        frame.setVisible(true);
        
        check.addItemListener(this);
    }

    public void itemStateChanged(ItemEvent ev) {
        String onOrOff = "off";
        if (check.isSelected()) onOrOff = "on";
        System.out.println("Check box is " + onOrOff);
    }
}
