public class PopSong 
{ //name of class is PopSong
    public static void main (String[] args) 
    { //name of method is main
        int popNum = 99; //an integer named popNum is assigned 99
        String word = "bottles"; // a String named word is assigned "bottles"

        while (popNum > 0) 
        {

            System.out.println(popNum + " " + word + " of pop on the wall");
            System.out.println(popNum + " " + word + " of pop.");
            System.out.println("Take one down.");
            System.out.println("Pass it around.");
            popNum = popNum - 1;

            if (popNum == 1) 
            {
                word = "bottle";
            }

            if (popNum > 0) 
            {
                System.out.println(popNum + " " + word + " of pop on the wall");
            } 
            else 
            {
                System.out.println("No more bottles of pop on the wall");
            }
        }
    }
}

