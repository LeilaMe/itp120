class XCopy {
    public static void main(String [] args) {
        int orig = 42;//make a variable and assign it 42
        XCopy x = new XCopy(); // make a new reference variable and object
        int y = x.go(orig); // make a variable y and assign it
        // x was not assigned?
        // nevermind it compiled
        System.out.println(orig + " " + y);
    }
    int go(int arg) {
        arg = arg * 2;
        return arg;
    }
}

