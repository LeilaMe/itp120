class Song {
    String title;
    String artist;

    void play() {
        System.out.println("Now Playing " + title + " by " + artist);
    }
}
class SongTestDrive {
    public static void main (String[] args) {
        Song one = new Song();
        one.title = "Never Gonna Give You Up";
        one.artist = "Rick Astley";

        one.play();
    }
}
