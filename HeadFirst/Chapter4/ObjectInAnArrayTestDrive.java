class ObjectInAnArray {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }

    void talk() {
        System.out.println("Hi, my name is " + name);
    }
}
class ObjectInAnArrayTestDrive {
    public static void main (String[] args) {
        ObjectInAnArray one = new ObjectInAnArray();
        one.setName("Uncle Bob");
        System.out.println("One: " + one.getName());
        one.talk();

        //Declare and create an array to hold 3 object references
        ObjectInAnArray[] a = new ObjectInAnArray[3];

        //Create new objects and assign them to array elements
        a[0] = new ObjectInAnArray();
        a[1] = new ObjectInAnArray();
        a[2] = a[0];

        //call methods on objects
        a[0].setName("Bingo");
        a[1].setName("Boop");
        a[2].setName("Bell");

        System.out.println("Objects in array are: " + a[0].getName() + " " + a[1].getName() + " " + a[2].getName());
    }
}
