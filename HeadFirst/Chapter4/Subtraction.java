class Subtraction {
    void go() {
        Subtraction one = new Subtraction();
        int soup = 9;
        int egg = 7;
        one.takeTwo(soup, egg);
    }

    void takeTwo(int x, int y) {
        int z = x - y;
        System.out.println("Difference is " + z);
    }
}

class SubtractionTestDrive {
    public static void main (String[] args) {
        Subtraction two = new Subtraction();
        two.go();
    }
}

